section .data
newl: db 10
numbers: db "0123456789"
negative: db "-"
charbuf:db ""

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit:
 mov rax, 60

    
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    	.loop:
		cmp byte [rdi+rax],0
		je .end
		inc rax
		jmp .loop

	.end:
		ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi
	call string_length
	pop rdi
	mov rdx, rax
    	mov rsi, rdi
    	mov rax, 1
    	mov rdi, 1
   	syscall
    ret


print_char:
    push rdi
    mov rdi, rsp
    call print_string
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rax, 1
	
	mov rsi, newl
	mov rdx, 1
	syscall
    	ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	xor rcx, rcx
	mov rax, rdi
	mov rbx, 10
	.loop:
		xor rdx, rdx
		inc rcx
		div rbx
		push rdx
		test rax, rax
		jnz .loop
	xor rbx, rbx
	.loop_print:
		dec rcx
		pop rdx
		lea rsi, [numbers+rdx]
		mov rax, 1
		mov rdi, 1
		mov rdx, 1
		push rcx
		syscall
		pop rcx
		test rcx, rcx
		jnz .loop_print
    	ret
; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	mov rax, rdi
	test rax, rax
	js .neg
	jmp print_uint
	.neg:
		mov rbx, rdi
		mov rax, 1
		mov rdi, 1
		mov rsi, negative
		mov rdx, 1
		syscall
		mov rax, rbx
		neg rax
		mov rdi, rax
		jmp print_uint
; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	push rdi
	push rsi
	call string_length
	pop rdi
	push rax
	mov rdx, rdi
	mov rdi, rsi
	call string_length
	pop rcx
	pop rsi
	push rdx
	cmp rcx, rax
	jne .unsuccessful
	test rax, rax
	je .successful
	pop rdx
	mov rsi, rdx
	mov rdx, rax
	xor rcx, rcx
	.cycle:
		push rdx
		mov  dl, [rdi+rcx]
		mov  al, [rsi+rcx]
		cmp rdx, rax
		jne .unsuccessful
		inc rcx
		pop rdx
		cmp rcx, rdx
		jne .cycle
	mov rax, 1
	ret
	.unsuccessful:
		pop rdx
		xor rax, rax
		ret
	.successful:
		pop rdx
		mov rax, 1
		ret
		

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	xor rax, rax
	push rax
	mov rax, 0
	mov rdi, 0
	mov rsi, charbuf
	mov rdx, 1
	syscall
	test al, al
	
	je .zer
	pop rax
	mov al, [charbuf]
	
    ret 
	.zer:
		
		xor rax, rax
		pop rax
		ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	push rsi
	xor rcx, rcx
	push rcx
	.check_space:
		push rdi
		call read_char
		test rax, rax
		je .null
		pop rdi
		mov al, [rsi]
		cmp rax, "\n"
		je .check_space
		cmp rax, 0x9
		je .check_space
		cmp rax, 0x20
		je .check_space
		mov byte [rdi], al
	.loop_read:
		pop rcx
		pop rax
		cmp rax, rcx
		je .err
		push rax
		push rcx
		push rdi
		call read_char
		mov rdx, rax
		pop rdi
		pop rcx
		mov al, [rsi]
		inc rcx
		mov byte [rdi+rcx],al
		push rcx
		mov rax, rdx
		cmp rax, '\n'
		je .end
		cmp rax, 9
		je .end
		cmp rax, 0x20
		je .end
		test rax, rax
		jnz .loop_read
	.end:
		pop rcx 
		mov byte [rdi+rcx], 0
		pop rdx
		mov rdx, rcx
		mov rax, rdi
		ret
	.null:
		pop rax
		pop rax
		pop rax
		xor rax, rax
		xor rdx, rdx
    	ret
	.err:
		xor rax, rax
		xor rdx, rdx
    	ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor rax, rax
	xor rcx, rcx
	mov al, [rdi+rcx]
	cmp al, '0'
	jl .no_num
	cmp al, '9'
	jg .no_num
	sub rax, '0'
	push rax
	inc rcx
	.loop_num:
		mov al, [rdi+rcx]
		cmp rax, '0'
		jl .end_num
		cmp al, '9'
		jg .end_num
		sub rax, '0'
		push rax
		inc rcx
		jmp .loop_num
	.no_num:
		xor rax, rax
		xor rdx, rdx
		ret
	.end_num:
		xor rdi, rdi
		mov rdx, rcx
	.end_loop:
		mov rax, 1
		mov rsi, rdx
		sub rsi, rcx
	.sqr:
		test rsi, rsi
		je .next
		imul rax, 10
		dec rsi
		jmp .sqr
	.next:
		pop rsi
		imul rax, rsi
		add rdi, rax
		dec rcx
		test rcx, rcx
		jne .end_loop                    
	mov rax, rdi
    	ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	xor rax, rax
    	mov al, [rdi]
	cmp al, [negative]
	je .negat
	cmp al, '0'
	jl .no_num
	cmp al, '9'
	jg .no_num
	sub rax, '0'
	call parse_uint
	ret
	.negat:
		inc rdi
		call parse_uint
		inc rdx
		neg rax
		ret
	.no_num:
		xor rax, rax
		xor rdx, rdx
		ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	call string_length
	cmp rax, rdx
	jg .err_size
	mov rcx, rax
	xor rdx, rdx
	.loop_copy:
		mov rax, [rdi+rdx]
  		mov qword [rsi+rdx], rax
		xor rax, rax
		cmp rcx, rax
		jg .mov_cycle
		jmp .next_copy
	.mov_cycle:
		sub rcx, rdx
		add rdx, 8
		jmp .loop_copy
	.next_copy:
		mov rdi, rsi
		jmp string_length
		
	.err_size:
		xor rax, rax
		ret

